package com.example.mszabi.bigproject;

/**
 * Created by nemes on 2018. 01. 03..
 */

/**
 * This class is the type of the advertisement.
 */
public class AdvertisementUploadInfo {

    public String Title, Description;

    public String imageURL;

    public AdvertisementUploadInfo() {

    }

    public AdvertisementUploadInfo(String name, String desc, String url) {

        this.Title = name;
        this.Description = desc;
        this.imageURL= url;
    }

    public String getTitle() {
        return Title;
    }

    public String getDescription() {
        return Description;
    }

    public String getImageURL() {
        return imageURL;
    }
}
