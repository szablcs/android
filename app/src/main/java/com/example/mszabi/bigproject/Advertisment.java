package com.example.mszabi.bigproject;

/**
 * Created by M.Szabi on 03.01.2018.
 */

public class Advertisment {
    private String Title, Description, url;

    public Advertisment() {
    }

    public Advertisment(String Title, String Description, String url) {
        this.Title = Title;
        this.Description = Description;
        this.url = url;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String name) {
        this.Title = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }
}

