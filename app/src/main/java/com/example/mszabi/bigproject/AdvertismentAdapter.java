package com.example.mszabi.bigproject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class AdvertismentAdapter extends RecyclerView.Adapter<AdvertismentAdapter.MyViewHolder> {
    private Context context;
    private List<Advertisment> advertismentsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView image;
        public TextView description;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            description = (TextView) view.findViewById(R.id.description);
            image = (ImageView) view.findViewById(R.id.image);
        }
    }


    public AdvertismentAdapter(List<Advertisment> advertismentsList, Context context) {
        this.advertismentsList = advertismentsList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.advertisment_row_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("UnsafeDynamicallyLoadedCode")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Advertisment advertisment = advertismentsList.get(position);

        holder.title.setText(advertisment.getTitle());
        holder.description.setText(advertisment.getDescription());
        Glide.with(context)
            .load(advertisment.getUrl())
                .placeholder(R.drawable.no_image)
            .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return advertismentsList.size();
    }
}
